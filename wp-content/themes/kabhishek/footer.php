
    <!--
    ==============
    * JS Files *
    ==============
    -->
    
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    
    <!-- jQuery -->
    <script src="<?=get_template_directory_uri()?>/resources/assets/plugins/js/jquery.min.js"></script>
    <!-- popper -->
    <script src="<?=get_template_directory_uri()?>/resources/assets/plugins/js/popper.min.js"></script>
    <!-- bootstrap -->
    <script src="<?=get_template_directory_uri()?>/resources/assets/plugins/js/bootstrap.min.js"></script>
    <!-- owl carousel -->
    <script src="<?=get_template_directory_uri()?>/resources/assets/plugins/js/owl.carousel.js"></script>
    <!-- validator -->
    <script src="<?=get_template_directory_uri()?>/resources/assets/plugins/js/validator.min.js"></script>
    <!-- wow -->
    <script src="<?=get_template_directory_uri()?>/resources/assets/plugins/js/wow.min.js"></script>
    <!-- mixin js-->
    <script src="<?=get_template_directory_uri()?>/resources/assets/plugins/js/jquery.mixitup.min.js"></script>
    <!-- circle progress-->
    <script src="<?=get_template_directory_uri()?>/resources/assets/plugins/js/circle-progress.js"></script>
    <!-- jquery nav -->
    <script src="<?=get_template_directory_uri()?>/resources/assets/plugins/js/jquery.nav.js"></script>
    <!-- Fancybox js-->
    <script src="<?=get_template_directory_uri()?>/resources/assets/plugins/js/jquery.fancybox.min.js"></script>
    <!-- isotope js-->
    <script src="<?=get_template_directory_uri()?>/resources/assets/plugins/js/isotope.pkgd.js"></script>
    <script src="<?=get_template_directory_uri()?>/resources/assets/plugins/js/packery-mode.pkgd.js"></script>
    <!-- Map api -->
    <script src="http://maps.googleapis.com/maps/api/js?v=3.exp&amp;key=AIzaSyCRP2E3BhaVKYs7BvNytBNumU0MBmjhhxc"></script>
    <!-- Custom Scripts-->
    <script src="<?=get_template_directory_uri()?>/resources/assets/js/map-init.js"></script>
    <script src="<?=get_template_directory_uri()?>/resources/assets/js/custom-scripts.js"></script>
    
 <!-- ****************
      After neccessary customization/modification, Please minify 
      JavaScript/jQuery according to http://browserdiet.com/en/ for better performance
    **************** -->
    <!-- STYLE SWITCH STYLESHEET ONLY FOR DEMO -->
    <link rel="stylesheet" href="<?=get_template_directory_uri()?>/resources/demo/demo.css">
    <script type="text/javascript" src="<?=get_template_directory_uri()?>/resources/demo/styleswitcher.js"></script>
    <script type="text/javascript" src="<?=get_template_directory_uri()?>/resources/demo/demo.js"></script>
        <div class="demo-style-switch" id="switch-style">
    <a id="toggle-switcher" class="switch-button"><i class="fa fa-snowflake-o fa-spin"></i></a>
    <div class="switched-options">
    
    <div class="config-title">
        Pick Colors :
    </div>
    <ul class="styles">
        <li><a href="#" onclick="setActiveStyleSheet('blue'); return false;" title="Light Purple">
            <div class="blue"></div> </a>
        </li>
        <li><a href="#" onclick="setActiveStyleSheet('purple'); return false;" title="Slate Magenta">
            <div class="purple"></div></a>
        </li>
        <li><a href="#" onclick="setActiveStyleSheet('blue-munsell'); return false;" title="Sky Blue">
            <div class="blue-munsell"></div></a>
        </li>
        <li><a href="#" onclick="setActiveStyleSheet('orange'); return false;" title="Gold Yellow">
            <div class="orange"></div></a>
        </li>
        <li><a href="#" onclick="setActiveStyleSheet('slate'); return false;" title="Orchid Pink">
            <div class="slate"></div></a>
        </li>
        <li><a href="#" onclick="setActiveStyleSheet('green'); return false;" title="Light Green">
            <div class="green"></div></a>
        </li>
        <li><a href="#" onclick="setActiveStyleSheet('yellow'); return false;" title="bright blue">
            <div class="yellow"></div>
</a>
        </li>
        <li><a href="#" onclick="setActiveStyleSheet('red'); return false;" title="Sea Green">
            <div class="red"></div></a>
        </li>
    </ul>
    </div>
    </div>
  
</body>

</html>