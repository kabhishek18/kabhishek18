<?php
$field = 'components';

while (have_rows($field)) {
  the_row();

  $layout = get_row_layout();
  // If the layout is a view
  if (in_array($layout, ['view'])) {
    $view = get_sub_field('view');
    @include(THEME_DIR . "/templates/components/view/$view.php");
  } elseif (in_array($layout, ['reference'])) {
    $reference = get_sub_field('reference');
    @include(THEME_DIR . "/templates/components/reference/$reference.php");
  }
  elseif (in_array($layout, ['tbd'])) {
    $tbd = get_sub_field('tbd');
    @include(THEME_DIR . "/templates/components/tbd/$tbd.php");
  }
  @include(THEME_DIR . "/templates/components/common/variables.php");
  @include(THEME_DIR . "/templates/components/$layout.php");
}
?>