 <?php if( have_rows('item') ):?>   

    <!--
           PRICING
        -->
        <section class="mh-pricing" id="ka-pricing">
            <div class="">
                <div class="container">
                    <div class="row section-separator">
                        <div class="col-sm-12 section-title" data-wow-duration="0.8s" data-wow-delay="0.2s">
                            <h3>Pricing Table</h3>
                        </div>
                        

                                    <?php
                        while ( have_rows('item') ) : the_row();?>
                        <div class="col-sm-12 col-md-4">
                            <div class="mh-pricing dark-bg shadow-2 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                        
                               <?=the_sub_field('body');?>
                               <?php $button=get_sub_field('button');?> 
                                <a href="<?=$button[0]['url']?>" class="btn <?=$button[0]['button_style']?>"><?=$button[0]['title']?></a>
                            </div>  
                        </div>                    
                        <?php endwhile;?>
                        
                    </div>
                </div>
            </div>
        </section>
<?php     
 else :
    endif;
?>              