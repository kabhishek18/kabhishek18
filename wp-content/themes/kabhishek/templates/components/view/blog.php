
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/simplePagination.js/1.6/jquery.simplePagination.js"></script>
        <!-- home 2 -->
        <section class="mh-home image-bg home-2-img" id="mh-home">
            <div class="img-foverlay img-color-overlay">
                <div class="container">
                    <div class="row section-separator">
                        <div class="mh-page-title text-center col-sm-12">
                            <h2><u>Blog</u></h2>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="mh-blog">
            <div class="container">
               <div class="list-wrapper wow">
 			  	<!-- Box Item -->

				<?php
				$args = array(
				'post_type'=> 'post',
				'orderby'    => 'ID',
				'post_status' => 'publish',
				'order'    => 'DESC',
				'posts_per_page' => -1 // this will retrive all the post that is published 
				);
				$result = new WP_Query( $args );
				$i =0;
				if ( $result-> have_posts() ) : ?>
				<?php while ( $result->have_posts() ) : $result->the_post(); ?>
					<?php $std =$result->posts;?>
					<?php $link =$std[$i]->guid;?>
					<!--For post single data-->
	              <div class="list-item">
		                <div class="row">
		                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		                      <div class="mh-about-inner">
		                          <div class="blogs-heading">
		                      		<h2 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.<?=$i?>s" style="visibility: visible; animation-duration: 0.8s; animation-delay: 0.<?=$i?>s; animation-name: fadeInUp;"><span>
		                      		<?php the_title(); ?>  </span></h2>
		                          </div>
		                          <div class="blogs-detail-text" style="margin-top: 15px;margin-bottom: 15px;">
		                            <p><?php the_excerpt()?>  </p>
		                          </div>
		                          <div class="post-meta-content">
		                            <span class="post-auhor-date">Posted on 
		                            <span style="color: #0bceaf;"><i class="far fa-calendar-alt"></i><?php $postdate =$std[$i]->post_date;echo date('F,d  Y',strtotime($postdate))?></span>
		                            </span>
		                            <span class="postby">
		                            <span> </span>
		                            <span class="by">by</span>
		                            <span  style="color: #0bceaf;"><?php the_author()?></span>
		                            </span>  
		                          </div>
		                          <div class="post_read_more" style="margin-top: 10px;">
		                            <a href="<?=$link?>"> <span style="color: #0bceaf;"><b>Read more</b></span> </a>
		                          </div>
		                      </div>
		                    </div>
		                    <??>
		                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		                      <div class="all-blogs-text-img">
		                      <img src="<?=get_the_post_thumbnail_url()?>">
		                      </div>
		                    </div>
		                </div>
	              </div>

				<?php $i++;endwhile; ?>
				<?php endif; wp_reset_postdata(); ?>



            	<!-- Box Item -->
             </div>
            </div>
        </section>       

<div id="pagination-container"></div>

<style type="text/css">

.list-wrapper {
	padding: 15px;
	overflow: hidden;
}

.list-item {
	border: 1px solid #EEE;
	margin-bottom: 10px;
	padding: 10px;
	box-shadow: 0px 0px 10px 0px #4a4a4a;
}

.list-item h2 {
	color: #FF7182;
	margin: 0 0 5px;	
}

.list-item p {
	margin: 0;
}
.all-blogs-text-img{
	overflow: hidden;
    width: 100%;
}
.simple-pagination ul {
	margin: 0 0 20px;
	padding: 0;
	list-style: none;
	text-align: center;
}

.simple-pagination li {
	display: inline-block;
	margin-right: 5px;
}

.simple-pagination li a,
.simple-pagination li span {
	color: #666;
	padding: 5px 10px;
	text-decoration: none;
	border: 1px solid #EEE;
	background-color: #FFF;
	box-shadow: 0px 0px 10px 0px #EEE;
}

.simple-pagination .current {
	color: #FFF;
	background-color: #100e17;
	border-color: #101e17;
}

.simple-pagination .prev.current,
.simple-pagination .next.current {
	background: #100e17;
}
</style>
<script type="text/javascript">
	// jQuery Plugin: http://flaviusmatis.github.io/simplePagination.js/

var items = $(".list-wrapper .list-item");
    var numItems = items.length;
    var perPage = 3;

    items.slice(perPage).hide();

    $('#pagination-container').pagination({
        items: numItems,
        itemsOnPage: perPage,
        prevText: "&laquo;",
        nextText: "&raquo;",
        onPageClick: function (pageNumber) {
            var showFrom = perPage * (pageNumber - 1);
            var showTo = showFrom + perPage;
            items.hide().slice(showFrom, showTo).show();
        }
    });
</script>
