 <?php if( have_rows('item') ):

?>   
  
         <!--
           PORTFOLIO
        -->
        <section class="mh-portfolio" id="ka-portfolio">
            <div class="container">
                <div class="row section-separator">
                    <div class="section-title col-sm-12 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.1s">
                        <h3>Recent Portfolio</h3>
                    </div>
                    <div class="part col-sm-12">
                        <div class="portfolio-nav col-sm-12" id="filter-button">
                            <ul>
                                <li data-filter="*" class="current wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.1s"> <span>All Categories</span></li>
                                <li data-filter=".application" class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s"><span>Application</span></li>
                                <li data-filter=".web-project" class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s"><span>Web Project</span></li>
                                <li data-filter=".branding" class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s"><span>Branding</span></li>
                                <li data-filter=".other" class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.5s"><span>Others</span></li>
                                
                            </ul>
                        </div>
                        <div class="mh-project-gallery col-sm-12 wow fadeInUp" id="project-gallery" data-wow-duration="0.8s" data-wow-delay="0.5s">
                            <div class="portfolioContainer row">
                                

                               <?php    $i=1;   while ( have_rows('item') ) : the_row();?>
                                   <?php 
                                    $title = get_sub_field('title');
                                    $subtitle = get_sub_field('subtitle'); 
                                    $select = get_sub_field('select');?>  
                                <div class="grid-item col-md-4 col-sm-6 col-xs-12 <?=$select?>">
                                    <figure>
                                     
                                    <?php $image = get_sub_field('image');
                                    if( !empty( $image ) ): ?>
                                    <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>"  />
                                    <?php endif; ?>
                                        <figcaption class="fig-caption">
                                            <i class="fa fa-search"></i>
                                            <h5 class="title"><?=$title?> </h5>
                                            <span class="sub-title"><?=$subtitle?></span>
                                            <a data-fancybox data-src="#<?=$i?>"></a>
                                        </figcaption>
                                    </figure>
                                </div>
                                  <?php $i++;endwhile;?>
                 
                         
                            </div> <!-- End: .grid .project-gallery -->
                        </div> <!-- End: .grid .project-gallery -->
                    </div> <!-- End: .part -->
                </div> <!-- End: .row -->
            </div>

 <?php      $i=1; while ( have_rows('item') ) : the_row();?>
            <div class="mh-portfolio-modal" id="<?=$i?>">
                <div class="container">
                    <div class="row mh-portfolio-modal-inner">
                        <div class="col-sm-5">
                             <?php 
                                    $heading = get_sub_field('heading');
                                    $blurb = get_sub_field('blurb');
                                    $url = get_sub_field('live_demo_url');
                                    $image1 = get_sub_field('extra-image1');
                                    $image2 = get_sub_field('extra-image2');?>
                            <h2><?=$heading?></h2>
                            <?=$blurb?>      
                            <div class="mh-about-tag">
                                <ul>
                                 <?php while ( have_rows('language') ) : the_row();
                                    $skill = get_sub_field('skill');?>
                                    <li><span><?=$skill?></span></li>
                                  <?php endwhile;?>  
                                </ul>
                            </div>
                            <a href="<?=$url?>" class="btn btn-fill" target="_blank">Live Demo</a>
                        </div>
                        <div class="col-sm-7">
                            <div class="mh-portfolio-modal-img">
      
                                <?php
                                    if( !empty( $image1 ) ): ?>
                                    <img src="<?php echo esc_url($image1['url']); ?>" alt="<?php echo esc_attr($image1['alt']); ?>"  class="img-fluid"/>
                                     <p>Image 1</p>   
                                    <?php endif; ?>
                                
                                 <?php
                                    if( !empty( $image2 ) ): ?>
                                    <img src="<?php echo esc_url($image2['url']); ?>" alt="<?php echo esc_attr($image2['alt']); ?>"  class="img-fluid"/>
                                     <p>Image 2</p>
                                    <?php else:endif; ?>
                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             <?php $i++; endwhile;?>

        </section>
<?php     
 else : echo "Protfolio Has Been Suspended for a while";
    endif;
?>                              