 <?php if( have_rows('item') ):?>   

    <!--     TESTIMONIALS -->
        <section class="mh-testimonial" id="ka-testimonial">
            <div class="home-v-img">
                <div class="container">
                    <div class="row section-separator">
                        <div class="col-sm-12 section-title wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                            <h3>Client Reviews</h3>
                        </div>
                        <div class="col-sm-12 wow fadeInUp" id="mh-client-review" data-wow-duration="0.8s" data-wow-delay="0.3s">
                        <?php
                        while ( have_rows('item') ) : the_row();?>
                            <div class="each-client-item">
                                <div class="mh-client-item dark-bg black-shadow-1">
                                  
                                    <?php 
                                    $image = get_sub_field('image');
                                    if( !empty( $image ) ): ?>
                                    <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="img-fluid" />
                                    <?php endif; ?>
                                    <p><?=the_sub_field('title');?></p>
                                    <h4><?=the_sub_field('author');?></h4>
                                    <span><?=the_sub_field('desigination');?>, <?=the_sub_field('company');?></span>
                                </div>
                            </div>

                        <?php endwhile;?>

                        </div>
                    </div>
                </div>
            </div>
        </section>
<?php     
 else :
    endif;
?>        