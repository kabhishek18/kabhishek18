
<!--
            ABOUT
        -->
        <section class="mh-about" id="ka-about">
            <div class="container">
                <div class="row section-separator">
                    <div class="col-sm-12 col-md-6">
                        <div class="mh-about-img shadow-2 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s">
                            <img src="<?=get_template_directory_uri()?>/resources/assets/images/ab-img.png" alt="" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="mh-about-inner">
                       
                            <h2 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.1s">About Me</h2>
                            <p class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                                <?=the_sub_field('body');?></p>
                            <div class="mh-about-tag wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s"><ul>
                                       <?php
                        while ( have_rows('language') ) : the_row();?>
                              <li> <span> <?=the_sub_field('skill');?></span></li>
                          <?php endwhile;?></ul>      
                             </div>    
                            <?php
                            $file = get_sub_field('file');
                            if( $file ): ?>
                            <a href="<?php echo $file['url']; ?>" class="btn btn-fill wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s">Downlaod CV <i class="fa fa-download"></i></a><?php endif; ?>
                             
                        </div>
                    </div>
                </div>
            </div>
        </section>


<!-- this is for  -->
<!-- <div class="mh-about-tag wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                             <ul>
    <li><span>PHP</span></li>
    <li><span>Html</span></li>
    <li><span>Css</span></li>
    <li><span>Codeiginter</span></li>
    <li><span>Laravel</span></li>
    <li><span>Wordpress</span></li>
    <li><span>Drupal</span></li>
    <li><span>Javascript</span></li>
    <li><span>Python</span></li>
    <li><span>Django</span></li>
    <li><span>AmazonWeb Service</span> </li>
</ul>
</div>   -->