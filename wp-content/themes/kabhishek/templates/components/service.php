 <?php if( have_rows('item') ):?>   
  <!--SERVICE -->
        <section class="mh-service" id="ka-service">
            <div class="container">
                <div class="row section-separator">
                    <div class="col-sm-12 text-center section-title wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                        <h2>What I do</h2>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="mh-service-item shadow-1 dark-bg wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                            <i class="fa fa-bullseye purple-color"></i>
                            <h3>UI / UX Design</h3>
                            <p>
                                A UX designer ensures a product makes sense to the user by creating a path that logically flows from one step to the next. A UI designer ensures each page visually communicates that path.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="mh-service-item shadow-1 dark-bg wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.5s">
                            <i class="fa fa-code iron-color"></i>
                            <h3>Web Development</h3>
                            <p>
                                A web developer or programmer is someone who takes a web design – which has been created by either a client or a design team – and turns it into a website.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <div class="mh-service-item shadow-1 dark-bg wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.7s">
                            <i class="fa fa-object-ungroup sky-color"></i>
                            <h3>App Development</h3>
                            <p>
                                Application developers identify ideas and concepts for the general public, or a specific need brought to them by a customer. We offers, mobile application development services.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section> 
<?php     
 else :
    endif;
?>                      