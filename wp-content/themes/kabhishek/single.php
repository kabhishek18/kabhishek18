<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>
<?php while ( have_posts()) : the_post();?>
			
			  <section class="mh-home image-bg home-2-img" id="mh-home">
            <div class="img-foverlay img-color-overlay">
                <div class="container">
                    <div class="row section-separator">
                        <div class="mh-page-title text-center col-sm-12">
                            <h2>Blog</h2>
                            <p>It is a long established fact that a reader will be <br>
                            distracted by the readable content of a page when looking at its layout.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

		  <section class="mh-blog">
            <div class="container">
                <div class="row section-separator">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-12 col-md-12">
                                <div class="mh-blog-item dark-bg blog-single">
                                    <img src="<?=get_the_post_thumbnail_url()?>" alt="blog-image" class="img-fluid" style="width: 100%">
                                    <div class="blog-inner">
                                        <h2><a href="#"><?php the_title();?></a></h2>
                                        <div class="mh-blog-post-info">
                                            <ul>
                                                <li><strong>Post On</strong><a href="#"><?php $post_date = get_the_date( 'l F j, Y' ); echo $post_date;?></a></li>
                                                <li><strong>By</strong><a href="#"><?php $nicename = get_the_author_meta( 'nicename', $author_id ); echo $nicename;?></a></li>
                                            </ul>
                                        </div>
                                       <?php the_content();?>
                                    </div>
                                </div>
                            </div>
                        
                        
                        </div>
                    </div>
                    
                    
                   
                    
                </div>
            </div>
        </section>       
  
       
        
 <?php endwhile; // End of the loop.?>

<?php
get_footer();
